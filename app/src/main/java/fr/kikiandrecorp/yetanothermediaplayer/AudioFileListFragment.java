package fr.kikiandrecorp.yetanothermediaplayer;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import fr.kikiandrecorp.yetanothermediaplayer.databinding.AudioFileListFragmentBinding;

public class AudioFileListFragment extends Fragment implements AudioFileListAdapter.OnAudioSelected {
    MyListener myListener;
    List<AudioFile> audioFileList = new ArrayList<>();

    int playlistPointer = -1;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        // this.populateFakeList();

        AudioFileListFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.audio_file_list_fragment,container,false);

        binding.audioFileList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        binding.audioFileList.setAdapter(new AudioFileListAdapter(audioFileList, this));


        Context context = getContext();

        // Internal content
        addAudioTracks(context, MediaStore.Audio.Media.INTERNAL_CONTENT_URI);

        // La carte SD
        addAudioTracks(context, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);


        return binding.getRoot();
    }

    public void setListener(MyListener listener) { this.myListener = listener;
    }

    private void addAudioTracks(Context context, Uri uri) {

        String[] projection = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DURATION,

        }; //chemin du fichier, titre, artist
        Cursor cursor = context.getContentResolver().query(uri,projection,null,null,null);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

            AudioFile audioFile = new AudioFile();

            audioFile.setFilePath(cursor.getString(0));
            audioFile.setTitle(cursor.getString(1));
            audioFile.setArtist(cursor.getString(2));
            audioFile.setAlbum(cursor.getString(3));
            audioFile.setDuration(cursor.getInt(4)/1000);

            audioFileList.add(audioFile);
        }
    }

    public int getPlaylistPointer() {
        if (this.playlistPointer == -1) {
            if (audioFileList.size() > 0) {
                playlistPointer = 0;
            }
        }

        return playlistPointer;
    }

    public AudioFile getPlaylistFile() {
        return this.audioFileList.get(this.getPlaylistPointer());
    }


    public void before() {
        int position = this.getPlaylistPointer();
        if (position > 0) {
            position--;
        }else {
            position = this.audioFileList.size()-1;
        }
        this.setAudioPointer(position++);
    }

    public void next() {
        int position = this.getPlaylistPointer();
        if (position < this.audioFileList.size()-1) {
            position++;
        } else {
            position = 0;
        }
        this.setAudioPointer(position++);
    }

    @Override
    public void setAudioPointer(int position) {
        this.playlistPointer = position;
        this.myListener.play();
    }
}