package fr.kikiandrecorp.yetanothermediaplayer;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.kikiandrecorp.yetanothermediaplayer.databinding.AudioFileItemBinding;

public class AudioFileListAdapter extends
        RecyclerView.Adapter<AudioFileListAdapter.ViewHolder> {
    List<AudioFile> audioFileList;

    OnAudioSelected callback;

    public AudioFileListAdapter(List<AudioFile> fileList, OnAudioSelected callback ) {
        assert fileList != null;
        this.audioFileList = fileList;
        this.callback = callback;

//        if (getItemCount() > 0) {
//            AudioFile test = fileList.get(0);
//        }
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AudioFileItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.audio_file_item, parent,false);
        return new ViewHolder(binding);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        AudioFile file = audioFileList.get(position);
        holder.viewModel.setAudioFile(file);

        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.setAudioPointer(position);
            }
        });
    }

    public interface OnAudioSelected {
        public void setAudioPointer(int position);
    }

    @Override
    public int getItemCount() {
        return audioFileList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        private AudioFileItemBinding binding;
        private AudioFileViewModel viewModel = new AudioFileViewModel();
        ViewHolder(AudioFileItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setAudioFileViewModel(viewModel);
        }
    }
}