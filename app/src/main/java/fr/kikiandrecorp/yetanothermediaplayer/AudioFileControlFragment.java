package fr.kikiandrecorp.yetanothermediaplayer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import fr.kikiandrecorp.yetanothermediaplayer.databinding.AudioFileControlFragmentBinding;

public class AudioFileControlFragment extends Fragment {

    private MyListener myListener;
    private boolean isPaused;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedIstanceState){
        final AudioFileControlFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.audio_file_control_fragment, container, false);


        binding.togglePlayPauseMusicBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                isPaused = myListener.togglePlayPauseMusic();
                // TODO : utiliser le boolean en PlayerService ispaused is play
                if(isPaused){
                    binding.togglePlayPauseMusicBtn.setText("PLAY");
                } else {
                    binding.togglePlayPauseMusicBtn.setText("PAUSE");

                }
            }
        });
        binding.beforeMusicBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                myListener.beforeMusic();

            }
        });
        binding.nextMusicBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                myListener.nextMusic();
            }
        });

        return binding.getRoot();
    }

    public void setListener(MyListener listener){ this.myListener = listener;}

}
