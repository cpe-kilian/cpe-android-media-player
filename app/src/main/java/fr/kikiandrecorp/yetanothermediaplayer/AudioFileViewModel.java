package fr.kikiandrecorp.yetanothermediaplayer;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import java.util.Locale;

public class AudioFileViewModel extends BaseObservable {
    private AudioFile audioFile = new AudioFile();
    public void setAudioFile(AudioFile file) {
        audioFile = file;
        notifyChange();
    }
    @Bindable
    public String getArtist() {
        return audioFile.getArtist();
    }
    @Bindable
    public String getTitle() {
        return audioFile.getTitle();
    }
    @Bindable
    public String getAlbum() {
        return audioFile.getAlbum();
    }
    @Bindable
    public String getDuration() {
        int duration = audioFile.getDuration();

        int second = duration % 60;
        int durationMinute = (duration - second) / 60;
        int minute = durationMinute % 60;
        int hour = (durationMinute - minute) / 60;
        if(hour > 0)
            return String.format(Locale.getDefault(),
                    "%02d:%02d:%02d",hour,minute,second);
        return String.format(Locale.getDefault(),
                "%02d:%02d",minute,second);
    }
}

