package fr.kikiandrecorp.yetanothermediaplayer;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.IOException;

public class PlayerService extends Service implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private final Binder binder = new PlayerBinder();
    private MediaPlayer mediaPlayer = new MediaPlayer();

    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY_COMPATIBILITY;
    }

    public void play(String path) throws IOException{

        mediaPlayer.reset();
        mediaPlayer.setDataSource(path);

        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.prepareAsync();
    }

    public void stop() {
        mediaPlayer.stop();
    }

    public void togglePause(){
        if (isPaused()) {
            mediaPlayer.start();
        } else {
            mediaPlayer.pause();
        }
    }

    public boolean haveMediaLoaded(){
        return mediaPlayer.getDuration() != -1;
    }

    public boolean isPaused(){
        return !isPlaying();
    }

    public boolean isPlaying(){
        try {
            return mediaPlayer.isPlaying();
        } catch (Exception e) {
            return false;
        }
    }


    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        this.mediaPlayer.setOnCompletionListener(listener);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent){
        return binder;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    public class PlayerBinder extends Binder{
        public PlayerService getService() {
            return PlayerService.this;
        }
    }
}
