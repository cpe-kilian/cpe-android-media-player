package fr.kikiandrecorp.yetanothermediaplayer;

public interface MyListener {

    public void play();

    public boolean togglePlayPauseMusic();

    public void beforeMusic();

    public void nextMusic();
}
