package fr.kikiandrecorp.yetanothermediaplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import fr.kikiandrecorp.yetanothermediaplayer.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements MyListener, MediaPlayer.OnCompletionListener{

    private ActivityMainBinding binding;

    PlayerService playerService;
    boolean playerBound = false;

    private AudioFileListFragment listFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        showStartup();
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();


        this.listFragment = new AudioFileListFragment();
        AudioFileControlFragment fragmentControl = new AudioFileControlFragment();

        listFragment.setListener(this);
        fragmentControl.setListener(this);

        transaction.replace(R.id.fragment_container,listFragment);
        transaction.replace(R.id.fragment_control, fragmentControl);

        transaction.commit();
    }

    @Override
    public void play() {
        AudioFile audioFile = this.listFragment.getPlaylistFile();
        try {
            playerService.play(audioFile.getFilePath());
        } catch (Exception e){
            //pas de bol
        }
    }

    @Override
    public boolean togglePlayPauseMusic() {
        if (playerService.haveMediaLoaded()) {
            playerService.togglePause();
        }

        return playerService.isPaused();

    }

    @Override
    public void beforeMusic() {
        this.listFragment.before();
        this.play();
    }


    @Override
    public void nextMusic() {
        this.listFragment.next();
        this.play();
    }


    @Override
    public void onStart() {
        super.onStart();
        Intent intent = new Intent(this, PlayerService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        unbindService(connection);
        playerBound = false;
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to PlayerService, cast the IBinder and get PlayerService instance
            PlayerService.PlayerBinder binder = (PlayerService.PlayerBinder) service;
            playerService = binder.getService();
            playerBound = true;
            playerService.setOnCompletionListener(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            playerBound = false;
        }
    };

    @Override
    public void onCompletion(MediaPlayer mp) {
        this.nextMusic();
    }
}
